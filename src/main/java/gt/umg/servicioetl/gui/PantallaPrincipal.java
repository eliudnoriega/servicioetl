package gt.umg.servicioetl.gui;

import gt.umg.servicioetl.dto.Alumno;
import gt.umg.servicioetl.dto.Asignacion;
import gt.umg.servicioetl.dto.Carrera;
import gt.umg.servicioetl.dto.Curso;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class PantallaPrincipal extends javax.swing.JFrame {

    private DefaultTableModel modelo = new DefaultTableModel();
    ArrayList<Alumno> listAlumnos = new ArrayList<>();
    ArrayList<Asignacion> listAsignacion = new ArrayList<>();
    ArrayList<Carrera> listCarrera = new ArrayList<>();
    ArrayList<Curso> listCurso = new ArrayList<>();
    JFileChooser abrirArchivo;
    String ruta = "";   
    String name = "";
    
    public PantallaPrincipal() {
        initComponents();
        modelo = (DefaultTableModel) jTable1.getModel();
    }

    public void muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        int line = 0;
        listAlumnos = new ArrayList<>();
        while ((cadena = b.readLine()) != null) {
            line = line + 1;
            String[] contenido = cadena.split(";");
            //System.out.println(line);
            if (line == 1) {
                ArrayList<String> nameColumn = new ArrayList<>();
                for (int i = 0; i < contenido.length; i++) {
                    nameColumn.add(contenido[i]);
                }
                modelo.setColumnIdentifiers(nameColumn.toArray());
            } else if (contenido.length > 0) {
                Object[] fila = new Object[contenido.length];
                Alumno a = new Alumno(contenido[0], contenido[1], contenido[2], contenido[3], contenido[4]);
                for (int i = 0; i < contenido.length; i++) {
                    fila[i] = contenido[i];
                    //System.out.println(contenido[i]);
                }
                listAlumnos.add(a);
                modelo.addRow(fila);
            }
        }
        //System.out.println(listAlumnos.toString());
        jTable1 = new javax.swing.JTable();
        jTable1.setModel(modelo);
        jTable1.updateUI();
        b.close();
    }

    public void cargaCarrera(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        listCarrera = new ArrayList<>();
        int line = 0;
        while ((cadena = b.readLine()) != null) {
            line = line + 1;
            String[] contenido = cadena.split(";");
            if (line == 1) {
                ArrayList<String> nameColumn = new ArrayList<>();
                for (int i = 0; i < contenido.length; i++) {
                    nameColumn.add(contenido[i]);
                }
                modelo.setColumnIdentifiers(nameColumn.toArray());
            } else if (contenido.length > 0) {
                Object[] fila = new Object[contenido.length];
                Carrera a = new Carrera(Integer.parseInt(contenido[0]), contenido[1]);
                for (int i = 0; i < contenido.length; i++) {
                    fila[i] = contenido[i];
                }
                listCarrera.add(a);
                modelo.addRow(fila);
            }
        }
        jTable1 = new javax.swing.JTable();
        jTable1.setModel(modelo);
        jTable1.updateUI();
        b.close();
    }

    public void cargaCurso(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        listCurso = new ArrayList<>();
        int line = 0;
        while ((cadena = b.readLine()) != null) {
            line = line + 1;
            String[] contenido = cadena.split(";");
            if (line == 1) {
                ArrayList<String> nameColumn = new ArrayList<>();
                for (int i = 0; i < contenido.length; i++) {
                    nameColumn.add(contenido[i]);
                }
                modelo.setColumnIdentifiers(nameColumn.toArray());
            } else if (contenido.length > 0) {
                Object[] fila = new Object[contenido.length];
                for (Carrera car : listCarrera) {
                    if (Integer.parseInt(contenido[2]) == car.getIdcarrera()) {
                        Curso a = new Curso(Integer.parseInt(contenido[0]), contenido[1], car);
                        listCurso.add(a);
                    }
                }
                for (int i = 0; i < contenido.length; i++) {
                    fila[i] = contenido[i];
                }
				modelo.addRow(fila);
            }
        }
        jTable1 = new javax.swing.JTable();
        jTable1.setModel(modelo);
        jTable1.updateUI();
        b.close();
    }
    
    public void cargaAsignacion(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        listAsignacion = new ArrayList<>();
        int line = 0;
        while ((cadena = b.readLine()) != null) {
            line = line + 1;
            String[] contenido = cadena.split(";");
            if (line == 1) {
                ArrayList<String> nameColumn = new ArrayList<>();
                for (int i = 0; i < contenido.length; i++) {
                    nameColumn.add(contenido[i]);
                }
                modelo.setColumnIdentifiers(nameColumn.toArray());
            } else if (contenido.length > 0) {
                Object[] fila = new Object[contenido.length];
                for (Alumno alum : listAlumnos) {
                    if (contenido[1].equals(alum.getCarnet())) {
                        for (Curso cur : listCurso) {
                            if (Integer.parseInt(contenido[2]) == cur.getIdCurso()) {
                                Asignacion a = new Asignacion(Integer.parseInt(contenido[0]), alum, cur, Double.parseDouble(contenido[4]));
                                listAsignacion.add(a);
                            }
                        }
                    }
                }
                for (int i = 0; i < contenido.length; i++) {
                    fila[i] = contenido[i];
                }
                modelo.addRow(fila);
            }
        }
        jTable1 = new javax.swing.JTable();
        jTable1.setModel(modelo);
        jTable1.updateUI();
        b.close();
    }

    public void eliminar() {        
        modelo.setRowCount(0);
        modelo.setColumnCount(3);
        jTable1.updateUI();
    }

     @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jBtn1 = new javax.swing.JButton();
        jBtn2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", ""
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jBtn1.setText("Cargar Archivos");
        jBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn1ActionPerformed(evt);
            }
        });

        jBtn2.setText("Alumnos por Genero");
        jBtn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtn2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jBtn1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBtn2)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jBtn1)
                    .addComponent(jBtn2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn1ActionPerformed
        jBtn1 = (JButton) evt.getSource();
        if (jBtn1.getText().equals("Cargar Archivos")) {
            if (abrirArchivo == null) {
                abrirArchivo = new JFileChooser();
            }
        }
        abrirArchivo.setAcceptAllFileFilterUsed(false);
        abrirArchivo.setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
        abrirArchivo.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int seleccion = abrirArchivo.showOpenDialog(this);
        if (seleccion == JFileChooser.APPROVE_OPTION) {            
            File f = abrirArchivo.getSelectedFile();
            try {
                eliminar();
                ruta = f.getAbsolutePath();
                name = f.getName();
            } catch (Exception exp) {
            }
        if (("Alumno.txt").equals(name) && !"".equals(ruta)) {
            try {
                muestraContenido(ruta);
            } catch (IOException ex) {
                Logger.getLogger(PantallaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(("Carrera.txt").equals(name) && !"".equals(ruta)) {
            try {
                cargaCarrera(ruta);
            } catch (IOException ex) {
                Logger.getLogger(PantallaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(("Curso.txt").equals(name) && !"".equals(ruta)) {
            try {
                cargaCurso(ruta);
            } catch (IOException ex) {
                Logger.getLogger(PantallaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else if(("Asignacion.txt").equals(name) && !"".equals(ruta)) {
            try {
                cargaAsignacion(ruta);
            } catch (IOException ex) {
                Logger.getLogger(PantallaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jBtn1ActionPerformed
}
    
    private void jBtn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtn2ActionPerformed
    jBtn2 = (JButton) evt.getSource();
        if (jBtn2.getText().equals("Alumnos por Genero")) {
            Alumno alum = new Alumno();
            System.out.println(alum.getNombre());
                
        }        
    }//GEN-LAST:event_jBtn2ActionPerformed
        

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtn1;
    private javax.swing.JButton jBtn2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
