/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.umg.servicioetl.dto;

/**
 *
 * @author melvin.noriega@megapaca.com
 */
public class Curso {

    private Integer idCurso;
    private String nombre;
    private Carrera carrera;

    public Curso() {
    }

    public Curso(Integer idCurso, String nombre, Carrera carrera) {
        this.idCurso = idCurso;
        this.nombre = nombre;
        this.carrera = carrera;
    }

    public Integer getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(Integer idCurso) {
        this.idCurso = idCurso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Carrera getCarrera() {
        return carrera;
    }

    public void setCarrera(Carrera carrera) {
        this.carrera = carrera;
    }

}
