/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.umg.servicioetl.dto;

/**
 *
 * @author melvin.noriega@megapaca.com
 */
public class Carrera {

    private Integer idcarrera;
    private String nombre;

    public Carrera() {
    }

    public Carrera(Integer idcarrera, String nombre) {
        this.idcarrera = idcarrera;
        this.nombre = nombre;
    }

    public Integer getIdcarrera() {
        return idcarrera;
    }

    public void setIdcarrera(Integer idcarrera) {
        this.idcarrera = idcarrera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

}
