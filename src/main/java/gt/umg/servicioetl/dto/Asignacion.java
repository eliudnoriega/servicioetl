/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gt.umg.servicioetl.dto;

/**
 *
 * @author melvin.noriega@megapaca.com
 */
public class Asignacion {

    private Integer idAsignacion;
    private Alumno alumno;
    private Curso curso;
    private Double nota;

    public Asignacion() {
    }

    public Asignacion(Integer idAsignacion, Alumno alumno, Curso curso, Double nota) {
        this.idAsignacion = idAsignacion;
        this.alumno = alumno;
        this.curso = curso;
        this.nota = nota;
    }

    public Integer getIdAsignacion() {
        return idAsignacion;
    }

    public void setIdAsignacion(Integer idAsignacion) {
        this.idAsignacion = idAsignacion;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

}
